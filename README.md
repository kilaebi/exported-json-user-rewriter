# Exported Json User Rewriter

Rewrite user info script in exported project json file 

# Prerequesites

* exported GitLab project archive file
    * generate from Settings -> General -> Advanced -> Export project
    * File Name Format: YYYY-MM-DD_HH-MM-SSS_[PJ_NAME].tar.gz
* jq (verified only ver 1.5.1)
* bash
* user.tsv (See below for detail)
    * Place this to current directory

# Usage

   gitlab-exported-json-user-rewriter.sh [options] [target] [target] ...

* Target:
    * relative path to exported GitLab project archive file(*.tar.gz)
* Options:
    *    --debug, -d       debug mode (leave intermediate files)
    *    --help, -h        print this

# Output

Userinfo rewrited archive (named [ORIGINAL_ARCHIVENAME]_rewrited.tar.gz).
Use this for project import.

# user.tsv

User mapping file (Old GitLab Instance <-> New GitLab Instance)

## format

```
[Full Name(src)]<TAB>[Full Name(dst)]<TAB>[Email(src)]<TAB>[Email(dst)]<TAB>[User Name(src)]<TAB>[User Name(dst)]
```

* write one line per one user
* empty line and lines beginning with a # are ignored

## Example

```tsv
# --------------------------
# Format:
# ---
# [Full Name(src)]<TAB>[Full Name(dst)]<TAB>[Email(src)]<TAB>[Email(dst)]<TAB>[User Name(src)]<TAB>[User Name(dst)]
# --------------------------

# A Group Member
John Smith   JohnSmith@example.com JohnSmith@example.com JohnSmith@example.com John Smith John.Smith
Mary William  MaryWilliam@example.com MaryWilliam@example.com MaryWilliam@example.com Mary William Mary.William

# B Group Member
Fred Bloggs  FredBloggs@example.com FredBloggs@example.com FredBloggs@example.com Fred Bloggs Fred.Bloggs
```

## Notes

This script is verified only exported by GitLab 12.4/12.5.
