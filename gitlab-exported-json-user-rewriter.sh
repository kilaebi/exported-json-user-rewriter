#!/bin/bash

set -e

USERINFO_FILE="user.tsv"
JSONFILE_BASE="project"
JSONFILE_ORIG_FILE="${JSONFILE_BASE}_orig.json"

function usage {
    cat <<EOF
$(basename ${0}) : A tool for rewriting user info in an exported GitLab project archive

Usage:
    $(basename ${0}) [<options>] [target] [target] ...

Target:
    exported GitLab project archive (*.tar.gz)

Options:
    --debug, -d       debug mode (leave intermediate files)
    --help, -h        print this
EOF
}

# ------ check file or directory -----
function rewrite_data_or_dir {
  echo "===== Processing ${1} ... ====="
      
  if [  ! -f "$USERINFO_FILE" ]; then
    echo "ERROR: Not found $USERINFO_FILE"
    exit 1
  fi 

  if [ -f "$1" ]; then
    extract_archive "$(pwd)/${1}" "$(pwd)/${USERINFO_FILE}"
  else
    if [ -d "$1" ]; then
      for file in $(find $1 -maxdepth 1 -type f); do
        extract_archive "$file"
      done
    else
      echo "ERROR: Invalid argument $1"
      exit 1
    fi
  fi

  echo "===== ${1} has rewrited successfully ! ====="
  echo
}

# ------ extract archive -----
function extract_archive {
  WORK="${1}__work"
  BASENAME=${1%.tar.gz}

  if [ -d $WORK ]; then
    rm -rf $WORK
  fi

  mkdir -p $WORK
  tar zxf $1 -C $WORK && err=0 || err=1
  
  if [ $err -eq 0 ]; then
    cd $WORK
    rewrite_userinfo "$WORK" "$2"
    if [ -f ${BASENAME}_rewrited.tar.gz ]; then
      rm ${BASENAME}_rewrited.tar.gz
    fi
    tar --exclude=${JSONFILE_ORIG_FILE} -czf ${BASENAME}_rewrited.tar.gz *
    cd ..
  else
    echo "ERROR: Invalid archive"
  fi
  rm -rf $WORK
}

# ----- rewrite userinfo -----
function rewrite_userinfo {
  LINE=0
  JSONFILE_BASE_PATH="${1}/project"
  JSONFILE="${JSONFILE_BASE_PATH}.json"
  JSONFILE_ORIG="${JSONFILE_BASE_PATH}_orig.json"
  WORKFILE_PREFIX="${JSONFILE_BASE_PATH}_work"
  PREV_WORKFILE=${JSONFILE_ORIG}

  if [ ! -f $JSONFILE_ORIG ]; then
    if [ ! -f $JSONFILE ]; then
      echo "ERROR: Not found target json file in archive!"
      echo "       Do you export archive from Settings -> General -> Advanced -> Export project Menu?"
      echo "       Check project.json file existence in archive extract directory '${WORKFILE_PREFIX}'"
      exit 1
    fi
    cp ${JSONFILE} ${JSONFILE_ORIG}
  else
    if [ -f $JSONFILE ]; then
      rm ${JSONFILE}
    fi
  fi

  rm -f ${WORKFILE_PREFIX}*

  while read row; do
    row=$(echo -e "$row" | tr -d "\r")
    (( LINE++ )) || true
    if [ "${row:0:1}" != "#" ] && [ -n "$(echo $row)" ]; then
      eval elem=("$(sed -e "s/'/'\\\\''/g" -e "s/\t/'\t'/g" -e "s/^/'/" -e "s/\s*$/'/" <<< "$row")")

      if [ ${#elem[@]} -ne 6 ]; then
        echo "ERROR: Invalid format in user.tsv line ${LINE}"
        exit 1
      fi

      WORKFILE=${WORKFILE_PREFIX}_${elem[5]}
      echo "replace Full name:  ${elem[0]} -> ${elem[1]} ..."
      cat $PREV_WORKFILE | jq --arg src "${elem[0]}" --arg dest "${elem[1]}" ' 
         .issues[].notes[].author.name    |= if . then gsub( $src; $dest ) else . end | 
         .snippets[].notes[].author.name  |= if . then gsub( $src; $dest ) else . end | 
         .project_members[].user.username |= if . then gsub( $src; $dest ) else . end | 
         .merge_requests[].notes[].author.name   |= if . then gsub( $src; $dest ) else . end | 
         .merge_requests[].merge_request_diff.merge_request_diff_commits[].author_name    |= if . then gsub( $src; $dest ) else . end | 
         .merge_requests[].merge_request_diff.merge_request_diff_commits[].committer_name |= if . then gsub( $src; $dest ) else . end 
        ' > ${WORKFILE}_fullname.json

      echo "replace email: ${elem[2]} -> ${elem[3]} ..."
      cat ${WORKFILE}_fullname.json | jq --arg src "${elem[2]}" --arg dest "${elem[3]}" ' 
         .project_members[].user.email    |= if . then gsub( $src; $dest ) else . end | 
         .merge_requests[].merge_request_diff.merge_request_diff_commits[].author_email    |= if . then gsub( $src; $dest ) else . end | 
         .merge_requests[].merge_request_diff.merge_request_diff_commits[].committer_email |= if . then gsub( $src; $dest ) else . end 
        ' > ${WORKFILE}_email.json

      echo "replace Username: ${elem[4]} -> ${elem[5]} ..."
      cat ${WORKFILE}_email.json | jq --arg src "${elem[4]}" --arg dest "${elem[5]}" \
        '.project_members[].user.username |= if . then gsub( $src; $dest ) else . end ' \
        |  sed -r "s|@${elem[4]}|@${elem[5]}|g"  > ${WORKFILE}_username.json

        echo ""
        PREV_WORKFILE="${WORKFILE}_username.json"
    fi
  done < ${2}

  cat ${PREV_WORKFILE} | jq -c . > ${JSONFILE}

  if [ -z "$DEBUG" ]; then
    rm -f ${WORKFILE_PREFIX}*
  fi

}

if [ $# -eq 0 ]; then
  usage
  exit 0
fi


while [ $# -gt 0 ];
do
    case ${1} in

        --debug|-d)
            DEBUG=1
        ;;

        --help|-h)
            usage
        ;;

        *)
            rewrite_data_or_dir "${1}"
        ;;
    esac
    shift
done

echo "Userinfo replacement completed!"
